# ANSYS Fluent Journal Scripts

Journal scripts (.jou) are used to automate ANSYS Fluent simulations. This repository containes typical journal scripts that can be used to automate common simulations, such as turbulent flows, reacting flows etc.

## Usage of the scripts

The scripts presented are used for a computational domain of Trapped Vortex Combustor (TVC). But, the script can be used for any simulations by copying the journal commands. The project will give an overview for users about the following,

1. Set boundary conditions.
2. Implement turbulence and combustion models.
3. Setup chemistry mechanisms.
4. Run and save results.
5. Post-process the results and save data in .csv format.
6. Lines, planes and poit data export for post processing.



