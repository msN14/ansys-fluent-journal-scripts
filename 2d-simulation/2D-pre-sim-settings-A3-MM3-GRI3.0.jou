"""
;******************************************************************************
                   			2D PRE SIMULATION SETTINGS
;..............................................................................
;DESCRIPTION OF THE FUNCTION:
;
Runs the setting for doing simulations

;..............................................................................
;REFERENCES:
;..............................................................................
;DEVELOPER INFORMATION: NISANTH M. S, m.nisanth.s@gmail.com
;******************************************************************************
;"""


;:::::::::::::::::::::::::::::::::::::::::::::[GENERAL SETTINGS]:::::::::::::::

;---------------------------------------------[Units]--------------------------
define/units/length mm
define/units/depth mm
define/units/mass-flow g/s

;---------------------------------------------[Reference Values]---------------
report reference-values compute velocity-inlet main_inlet
report reference-values depth 50
report reference-values length 300

;---------------------------------------------[Model Settings]-----------------
define/models/steady
define/models/solver/pressure-based yes

;---------------------------------------------[Enable energy equation]---------
/define/models/energy yes no no no yes

;---------------------------------------------[Turbulence Model Settings]------
define/models/viscous
ke-rng yes
near-wall-treatment/scalable-wall-functions yes
q

;---------------------------------------------[Chemkin Import]-----------------
file/import/chemkin-mechanism
"chemkin-import"
"E:\Simulations\Run\Input Files\Mechanism Files\RM4_gri53\grimech53.inp"
"E:\Simulations\Run\Input Files\Mechanism Files\RM4_gri53\thermo.dat"
no yes
"E:\Simulations\Run\Input Files\Mechanism Files\RM4_gri53\transport.dat"

;---------------------------------------------[Species Transport]--------------
define/models/species species-transport yes chemkin-import

;---------------------------------------------[ideal-gas law for density]------
define/materials change-create chemkin-import chemkin-import
no yes ideal-gas no no no no no

;---------------------------------------------[Convergence Criteria]-----------
solve/monitors/residual/convergence-criteria
1e-06 1e-06 1e-06 1e-06 1e-06 1e-06 1e-06 1e-06
1e-06 1e-06 1e-06 1e-06 1e-06 1e-06 1e-06 1e-06
1e-06 1e-06 1e-06 1e-06 1e-06 1e-06 1e-06 1e-06
1e-06 1e-06 1e-06 1e-06 1e-06 1e-06 1e-06 1e-06
1e-06 1e-06 1e-06 1e-06 1e-06 1e-06 1e-06 1e-06
1e-06 1e-06 1e-06 1e-06 1e-06 1e-06 1e-06 1e-06
1e-06 1e-06 1e-06 1e-06 1e-06 1e-06 1e-06 1e-06 1e-06 1e-06

;:::::::::::::::::::::::::::::::::::::::::::::[BOUNDARY CONDITIONS]::::::::::::

;TVC STATE CODE | P=1 | T=300 | V=10 | J=3.0 | ERC=2 | ERM=0 | ERO=0.26 |

;=============================================[MAIN INLET]=====================
define/boundary-conditions/
velocity-inlet
;---------------------------------------------[Velocity and Temperature]-------
main_inlet no no yes yes no 10 no 0 no 300
;---------------------------------------------[Intensity [%] and Scale [mm]]---
no yes 10 2 no
;---------------------------------------------[Y(O2),Y(CH4) and Y(N2)]---------
no 0 no 0 no 0 no 0.233
no 0 no 0 no 0 no 0 no 0 no 0 no 0 no 0 no 0 no 0
no 0 no 0 no 0 no 0 no 0 no 0 no 0 no 0 no 0 no 0 no 0 no 0
no 0 no 0 no 0 no 0 no 0 no 0 no 0 no 0 no 0 no 0 no 0 no 0 no 0 no 0
no 0 no 0 no 0 no 0 no 0 no 0 no 0 no 0.767
no 0 no 0 no 0 no 0  q

;=============================================[CAVITY INLET]===================
define/boundary-conditions/
velocity-inlet
;---------------------------------------------[Velocity and Temperature]-------
cavity_inlet no no yes yes no 18.03 no 0 no 300
;---------------------------------------------[Intensity [%] and Scale [mm]]---
no yes 10 0.14 no
;---------------------------------------------[Y(O2),Y(CH4) and Y(N2)]---------
no 0 no 0 no 0 no 0.2086
no 0 no 0 no 0 no 0 no 0 no 0 no 0 no 0 no 0 no 0.1046
no 0 no 0 no 0 no 0 no 0 no 0 no 0 no 0 no 0 no 0 no 0 no 0
no 0 no 0 no 0 no 0 no 0 no 0 no 0 no 0 no 0 no 0 no 0 no 0 no 0 no 0
no 0 no 0 no 0 no 0 no 0 no 0 no 0 no 0.6868
no 0 no 0 no 0 no 0  q

;=============================================[OUTLET]=========================
define/boundary-conditions/ 
pressure-outlet
;---------------------------------------------[Intensity [%] and Scale [mm]]---
outlet yes no 0 no 800 no yes no yes 10 2 no
;---------------------------------------------[Y(O2),Y(CH4) and Y(N2)]---------
no 0 no 0 no 0 no 0.233
no 0 no 0 no 0 no 0 no 0 no 0 no 0 no 0 no 0 no 0
no 0 no 0 no 0 no 0 no 0 no 0 no 0 no 0 no 0 no 0 no 0 no 0
no 0 no 0 no 0 no 0 no 0 no 0 no 0 no 0 no 0 no 0 no 0 no 0 no 0 no 0
no 0 no 0 no 0 no 0 no 0 no 0 no 0 no 0.767
no 0 no 0 no 0 no 0  q

;----------second 'no' is average pressure specification-----------------------
no no no q
q

;=============================================[WALLS]==========================
;define/boundary-conditions/set
;wall
;inlet_walls cavity_walls duct_top_wall duct_bottom_wall () q q q

;---------------------------------------------[OPERATING CONDITIONS]-----------
;----------------------------------------change 240 0 to 240 0 0 for 3D case---
define/operating-conditions
reference-pressure-location 240 0
operating-pressure 101325  q

;:::::::::::::::::::::::::::::::::::::::::::::[POINTS, LINES AND PLANES]:::::::

;---------------------------------------------[Points x0 y0 z0]----------------
;not required in stready simulations
surface/point-surface p1 10 0

surface/point-surface p4 135 5
surface/point-surface p3 135 0
surface/point-surface p2 135 -5

surface/point-surface p7 230 5
surface/point-surface p6 230 0
surface/point-surface p5 230 -5


;---------------------------------------------[Lines x0 y0 x1 y1]--------------
surface/line-surface cavity-line-1 10 10 10 -40
surface/line-surface cavity-line-2 20 10 20 -40
surface/line-surface main-line-1 -30 5 240 5
surface/line-surface main-line-2 -30 -5 240 -5
surface/line-surface outlet-line 230 -10 230 10

;---------------------------------------------[Planes x0y0 x1y1 x2y2]----
;not possible in 2D

;---------------------------------------------[Deletion syntax]----------------
;surface/delete-surface cavity-line1

;:::::::::::::::::::::::::::::::::::::::::::::[DATA MONITORING]::::::::::::::::

;---------------------------------------------[Clear All Monitors]-------------
solve/monitors/surface/clear-monitors

;---------------------------------------------[Setting Monitors]---------------
solve/monitors/surface/set-monitor
cavity-line1-velocity "Area-Weighted Average" velocity-magnitude 
cavity-line-1 () yes 2 no yes "cavity-line1-velocity" 1 q

solve/monitors/surface/set-monitor
cavity-line2-velocity "Area-Weighted Average" velocity-magnitude 
cavity-line-2 () yes 3 no yes "cavity-line2-velocity" 1 q

solve/monitors/surface/set-monitor
cavity-line1-k "Area-Weighted Average" turb-kinetic-energy 
cavity-line-1 () yes 4 no yes "cavity-line1-k" 1 q

solve/monitors/surface/set-monitor
cavity-line2-k "Area-Weighted Average" turb-kinetic-energy 
cavity-line-2 () yes 5 no yes "cavity-line2-k" 1 q 

solve/monitors/surface/set-monitor
cavity-line1-ch4 "Area-Weighted Average" ch4 
cavity-line-1 () yes 6 no yes "cavity-line1-ch4" 1 q

solve/monitors/surface/set-monitor
cavity-line2-ch4 "Area-Weighted Average" ch4 
cavity-line-2 () yes 7 no yes "cavity-line2-ch4" 1 q

;---------------------------------------------[Temperature Monitor]------------

;---------------------------------------------[Species Monitor]----------------

;:::::::::::::::::::::::::::::::::::::::::::::[DEFAULT SOLVER SETTINGS]::::::::

;---------------------------------------------[AUTOSAVE SETTINGS]--------------
file/auto-save/case-frequency/if-case-is-modified
file/auto-save/data-frequency/100
file/auto-save/root-name case

solve/set/number-of-iterations/100000

;---------------------------------------------[1ST ORDER NONREACTING RANS]-----
define/models/species/
species-transport yes chemkin-import volumetric-reactions no q

solve/set/set-all-species-together yes yes q
solve/set/discretization-scheme 
pressure/10 density/0 mom/0 k/0 epsilon/0 species-0/0 temperature/0  q

solve/set/under-relaxation species-0/1.0 temperature/1.0  q


;:::::::::::::::::::::::::::::::::::::::::::::[CASE MANAGE]::::::::::::::::::::

;---------------------------------------------[CASE INITIALIZATION]------------
/solve/initialize/hyb-initialization

;---------------------------------------------[CASE BACKUP SAVE]---------------
/file/write-case-data/case ok

q

;:::::::::::::::::::::::::::::::::::::::::::::[CASE RUNNING]:::::::::::::::::::

;<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<[Case Running]<<<<<<<<<<<<<<<<<<<
; manual running of the case required
;>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>[Case Finished]>>>>>>>>>>>>>>>>>>

;>> next step run the case

q