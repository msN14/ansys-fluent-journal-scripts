;"""
;******************************************************************************
;                              EXPORT FOR PERFORMANCE
;..............................................................................
;DESCRIPTION OF THE FUNCTION:
;>> Runs for extracting temperature and pollutant emission data

;..............................................................................
;REFERENCES:
;..............................................................................
;DEVELOPER INFORMATION: NISANTH M. S, m.nisanth.s@gmail.com
;******************************************************************************
;"""

; 1. Contour data for Temperature, Y[OH, CH4, NO, NO2], V[mag, x, y]
;    Turublence[k,epsilon]
; 2. Temperature profile at exit (x=230mm and -10 < Y < +10)
; 3. Temperature at 4 exit measurement points (P3, P5, P6, P7)
; 4. Average pressure/abs-pressure values
; 5. mdot species at outlet
; 6. Average species X at outlet


;:::::::::::::::::::::::::::::::::::::::::::::[PLANE RESULTS]::::::::::::::::::

;---------------------------------------------[Interior-2D]--------------------
file/export/ascii export-performance/midplane=all.data
interior-fluid_domain () yes
velocity-magnitude x-velocity y-velocity temperature
turb-kinetic-energy turb-diss-rate
ch4 o2 oh co2 co no no2 () no

q

;:::::::::::::::::::::::::::::::::::::::::::::[LINE RESULTS]:::::::::::::::::::

;---------------------------------------------[Lines x0 y0 x1 y1]--------------
;surface/line-surface outlet-line 230 -10 230 10

;---------------------------------------------[LINE DATA]----------------------
; temperature + pollutant emission
; create folder ./export-performance before running it

;---------------------------------------------[outlet-line]------------------
plot/plot yes
export-performance/outlet-line=temperature.data yes no no
temperature yes 0 1
outlet-line ()
;

q

;:::::::::::::::::::::::::::::::::::::::::::::[POINT RESULTS]::::::::::::::::::

;---------------------------------------------[Points x0 y0 z0]----------------

;---------------------------------------------[REPORTS]------------------------
; create folder ./export-performance before running

;---------------------------------------------[Point Data]---------------------
/report/surface-integrals/vertex-avg
p3 p5 p6 p7 ()
temperature yes "export-performance/outlet-points=temperature.data" q

;---------------------------------------------[Point Data]---------------------
/report/surface-integrals/area-weighted-avg
cavity_inlet main_inlet outlet ()
pressure yes "export-performance/inoutlet-points=pressure.data" q

/report/surface-integrals/area-weighted-avg
cavity_inlet main_inlet outlet ()
absolute-pressure yes "export-performance/inoutlet-points=pressure.data" q

;---------------------------------------------[Mass flow rates]----------------
/report/surface-integrals/flow-rate
cavity_inlet main_inlet outlet ()
o2 yes "export-performance/outlet=mdot-species.data" yes q

/report/surface-integrals/flow-rate
cavity_inlet main_inlet outlet ()
n2 yes "export-performance/outlet=mdot-species.data" yes q

/report/surface-integrals/flow-rate
cavity_inlet main_inlet outlet ()
co yes "export-performance/outlet=mdot-species.data" yes q

/report/surface-integrals/flow-rate
cavity_inlet main_inlet outlet ()
no yes "export-performance/outlet=mdot-species.data" yes q

/report/surface-integrals/flow-rate
cavity_inlet main_inlet outlet ()
no2 yes "export-performance/outlet=mdot-species.data" yes q

/report/surface-integrals/flow-rate
cavity_inlet main_inlet outlet ()
ch4 yes "export-performance/outlet=mdot-species.data"  q

/report/surface-integrals/flow-rate
cavity_inlet main_inlet outlet ()
ch yes "export-performance/outlet=mdot-species.data" yes q

/report/surface-integrals/flow-rate
cavity_inlet main_inlet outlet ()
ch2 yes "export-performance/outlet=mdot-species.data" yes q

/report/surface-integrals/flow-rate
cavity_inlet main_inlet outlet ()
ch3 yes "export-performance/outlet=mdot-species.data" yes q

/report/surface-integrals/flow-rate
cavity_inlet main_inlet outlet ()
c2h yes "export-performance/outlet=mdot-species.data" yes q

/report/surface-integrals/flow-rate
cavity_inlet main_inlet outlet ()
c2h2 yes "export-performance/outlet=mdot-species.data" yes q

/report/surface-integrals/flow-rate
cavity_inlet main_inlet outlet ()
c2h3 yes "export-performance/outlet=mdot-species.data" yes q

/report/surface-integrals/flow-rate
cavity_inlet main_inlet outlet ()
c2h4 yes "export-performance/outlet=mdot-species.data" yes q

/report/surface-integrals/flow-rate
cavity_inlet main_inlet outlet ()
c2h5 yes "export-performance/outlet=mdot-species.data" yes q

/report/surface-integrals/flow-rate
cavity_inlet main_inlet outlet ()
c2h6 yes "export-performance/outlet=mdot-species.data" yes q

/report/surface-integrals/flow-rate
cavity_inlet main_inlet outlet ()
c3h7 yes "export-performance/outlet=mdot-species.data" yes q

/report/surface-integrals/flow-rate
cavity_inlet main_inlet outlet ()
c3h8 yes "export-performance/outlet=mdot-species.data" yes q

;---------------------------------------------[Average Mole Fractions]---------
/report/surface-integrals/area-weighted-avg
cavity_inlet main_inlet outlet ()
molef-o2 yes "export-performance/outlet=x-species.data" yes q

/report/surface-integrals/area-weighted-avg
cavity_inlet main_inlet outlet ()
molef-n2 yes "export-performance/outlet=x-species.data" yes q

/report/surface-integrals/area-weighted-avg
cavity_inlet main_inlet outlet ()
molef-no yes "export-performance/outlet=x-species.data" yes q

/report/surface-integrals/area-weighted-avg
cavity_inlet main_inlet outlet ()
molef-no2 yes "export-performance/outlet=x-species.data" yes q

/report/surface-integrals/area-weighted-avg
cavity_inlet main_inlet outlet ()
molef-ch4 yes "export-performance/outlet=x-species.data"  q

/report/surface-integrals/area-weighted-avg
cavity_inlet main_inlet outlet ()
molef-co yes "export-performance/outlet=x-species.data" yes q

/report/surface-integrals/area-weighted-avg
cavity_inlet main_inlet outlet ()
molef-co2 yes "export-performance/outlet=x-species.data" yes q

q

;::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
