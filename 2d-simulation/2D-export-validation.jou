;"""
;******************************************************************************
                              EXPORT FOR VALIDATION
;..............................................................................
;DESCRIPTION OF THE FUNCTION:
;>> Runs for extracting temperature and pollutant emission data

;..............................................................................
;REFERENCES:
;..............................................................................
;DEVELOPER INFORMATION: NISANTH M. S, m.nisanth.s@gmail.com
;******************************************************************************
;"""

; 1. Temperature profile at exit (x=230mm and -10 < Y < +10)
; 2. Pollutant emission profile at exit (x=230mm and -10 < Y < +10)
; 3. Temperature at 3 exit measurement points (P5, P6, P7)
; 4. Pollutant emission at 3 exit measurement points (P5, P6, P7)

;:::::::::::::::::::::::::::::::::::::::::::::[LINE RESULTS]:::::::::::::::::::

;---------------------------------------------[Lines x0 y0 x1 y1]--------------
;surface/line-surface outlet-line 230 -10 230 10

;---------------------------------------------[LINE DATA]----------------------
; temperature + pollutant emission
; create folder ./export-validation before running it

;---------------------------------------------[outlet-line]------------------
plot/plot yes
export-validation/outlet-line=temperature yes no no
temperature yes 0 1
outlet-line ()
;
plot/plot yes
export-validation/outlet-line=xo2 yes no no
molef-o2 yes 0 1
outlet-line ()
;
plot/plot yes
export-validation/outlet-line=xco2 yes no no
molef-co2 yes 0 1
outlet-line ()
;
plot/plot yes
export-validation/outlet-line=xco yes no no
molef-co yes 0 1
outlet-line ()
;
plot/plot yes
export-validation/outlet-line=xch4 yes no no
molef-ch4 yes 0 1
outlet-line ()
;
plot/plot yes
export-validation/outlet-line=xn2 yes no no
molef-n2 yes 0 1
outlet-line ()
;
plot/plot yes
export-validation/outlet-line=xno yes no no
molef-no yes 0 1
outlet-line ()
;
plot/plot yes
export-validation/outlet-line=xno2 yes no no
molef-no2 yes 0 1
outlet-line ()
;

q

;:::::::::::::::::::::::::::::::::::::::::::::[POINT RESULTS]::::::::::::::::::

;---------------------------------------------[Points x0 y0 z0]----------------
;surface/point-surface p1 10 0
;surface/point-surface p2 135 -5
;surface/point-surface p3a 135 -1
;surface/point-surface p3 135 0
;surface/point-surface p3b 135 1
;surface/point-surface p4 135 5

;surface/point-surface p7t 230 6
;surface/point-surface p7 230 5
;surface/point-surface p7b 230 4

;surface/point-surface p6t 230 1
;surface/point-surface p6 230 0
;surface/point-surface p6b 230 -1

;surface/point-surface p5t 230 -4
;surface/point-surface p5 230 -5
;surface/point-surface p5b 230 -6

;---------------------------------------------[REPORTS]------------------------
; create folder ./export-validation before running

;---------------------------------------------[Point Data]---------------------
/report/surface-integrals/facet-avg
p3 p5 p6 p7 ()
temperature yes "export-validation/temperature.srp" q

;---------------------------------------------[Point Data]---------------------
/report/surface-integrals/facet-avg
p5 p6 p7 ()
molef-o2 yes "export-validation/mole-fraction-species.srp" q

/report/surface-integrals/facet-avg
p5 p6 p7 ()
molef-co2 yes "export-validation/mole-fraction-species.srp" yes q

/report/surface-integrals/facet-avg
p5 p6 p7 ()
molef-co yes "export-validation/mole-fraction-species.srp" yes q

/report/surface-integrals/facet-avg
p5 p6 p7 ()
molef-ch4 yes "export-validation/mole-fraction-species.srp" yes q


/report/surface-integrals/facet-avg
p5 p6 p7 ()
molef-n2 yes "export-validation/mole-fraction-species.srp" yes q

/report/surface-integrals/facet-avg
p5 p6 p7 ()
molef-no yes "export-validation/mole-fraction-species.srp" yes q

/report/surface-integrals/facet-avg
p5 p6 p7 ()
molef-no2 yes "export-validation/mole-fraction-species.srp" yes q


;---------------------------------------------[CxHy Related]-------------------
/report/surface-integrals/facet-avg
p5 p6 p7 ()
molef-ch4 yes "export-validation/mole-fraction-species-cxhy.srp" q

/report/surface-integrals/facet-avg
p5 p6 p7 ()
molef-ch yes "export-validation/mole-fraction-species-cxhy.srp" yes q

/report/surface-integrals/facet-avg
p5 p6 p7 ()
molef-ch2 yes "export-validation/mole-fraction-species-cxhy.srp" yes q

/report/surface-integrals/facet-avg
p5 p6 p7 ()
molef-ch3 yes "export-validation/mole-fraction-species-cxhy.srp" yes q

/report/surface-integrals/facet-avg
p5 p6 p7 ()
molef-c2h yes "export-validation/mole-fraction-species-cxhy.srp" yes q

/report/surface-integrals/facet-avg
p5 p6 p7 ()
molef-c2h2 yes "export-validation/mole-fraction-species-cxhy.srp" yes q

/report/surface-integrals/facet-avg
p5 p6 p7 ()
molef-c2h3 yes "export-validation/mole-fraction-species-cxhy.srp" yes q

/report/surface-integrals/facet-avg
p5 p6 p7 ()
molef-c2h4 yes "export-validation/mole-fraction-species-cxhy.srp" yes q

/report/surface-integrals/facet-avg
p5 p6 p7 ()
molef-c2h5 yes "export-validation/mole-fraction-species-cxhy.srp" yes q

/report/surface-integrals/facet-avg
p5 p6 p7 ()
molef-c2h6 yes "export-validation/mole-fraction-species-cxhy.srp" yes q

/report/surface-integrals/facet-avg
p5 p6 p7 ()
molef-c3h7 yes "export-validation/mole-fraction-species-cxhy.srp" yes q

/report/surface-integrals/facet-avg
p5 p6 p7 ()
molef-c3h8 yes "export-validation/mole-fraction-species-cxhy.srp" yes q

q

;::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
